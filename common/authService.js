var jwt = require('jsonwebtoken');
var Account = require('../domain/Account');
const JWT_SECRET = "0b5b5f0a-9aa0-45c2-9ab4-9381bdc57ac2";

var Auth = function() {
};

Auth.prototype.verify = function(authToken, cb) {
  let token = "";
  if (authToken) {
    token = authToken.replace("Bearer ", "");
  }
  jwt.verify(token, JWT_SECRET, cb);
};

Auth.prototype.login = function(username, password, cb) {

  Account.findOne({username, password}).exec().then((matchingUser) => {
    if (matchingUser) {
      jwt.sign({username: matchingUser.username}, JWT_SECRET, {expiresIn: "1d"}, cb);
    } else {
      cb(new Error("Auth failed"));
    }
  });
};

Auth.prototype.register = function(username, password, cb) {

  if (username && password) {
    Account.findOne({username, password}).exec().then((matchingUser) => {
      if (matchingUser === null) {
        var newAccount = new Account({username, password});
        newAccount.save()
            .then(() => cb(null))
            .catch((err) => cb(new Error("Registration failed")));
      } else {
        cb(new Error("Registration failed"));
      }
    });

  } else {
    cb(new Error("Registration failed"));
  }
};


module.exports = new Auth();