var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var Schema = mongoose.Schema;
var schema = new Schema({ name: String, email: String, belongsTo: String });

schema.plugin(autoIncrement.plugin, { model: 'Contact', field: 'id' });
module.exports = mongoose.model('Contact', schema);