var express = require('express');
var router = express.Router();
var authService = require('../common/authService');

router.post('/', function(req, res, next) {
  let password = req.body.password;
  let username = req.body.username;

  authService.login(username, password, (err, token) => {
    if (err) {
      res.status(400).send("Authentication failed");
    } else {
      res.send({token, username});
    }
  });

});

router.post('/register', function (req, res, next) {
  let password = req.body.password;
  let username = req.body.username;

  authService.register(username, password, err => {
    if (err) {
      res.status(400).send("Could not register");
    } else {
      res.status(204).send();
    }
  })
});

module.exports = router;
