var express = require('express');
var router = express.Router();

var Contact = require('../domain/Contact');

/* GET home page. */
router.get('/', function(req, res, next) {
  Contact.find({belongsTo: req.jwt.username}).then(contacts => res.json(contacts || []));
});

router.get('/:id', function(req, res, next) {
  Contact.findOne({id: parseInt(req.params.id), belongsTo: req.jwt.username})
      .then(contact => {
        contact ? res.json(contact) : res.status(404).end();
      });
});

router.post('/', function(req, res, next) {
  let newContact = new Contact({
    name: req.body.name,
    email: req.body.email,
    belongsTo: req.jwt.username
  });
  newContact.save().then(contact => {
    contact._id = undefined;
    contact.__v = undefined;
    res.json(contact);
  });
});

router.put('/:id', function(req, res, next) {

  Contact.update({ id: req.params.id, belongsTo: req.jwt.username}, { $set: {name, email} = req.body}).then(() => {
    Contact.findOne({id: parseInt(req.params.id), belongsTo: req.jwt.username})
        .then(contact => {
          contact ? res.json(contact) : res.status(404).end();
        });
  });


});

router.delete('/:id', function(req, res, next) {
  Contact.findOne({id: parseInt(req.params.id), belongsTo: req.jwt.username}).remove(() => {
    res.status(204).send();
  });
});

module.exports = router;